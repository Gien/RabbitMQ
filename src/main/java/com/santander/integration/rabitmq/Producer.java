package com.santander.integration.rabitmq;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.http.MediaType;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
 
public class Producer {
  
 private final static String MESSAGE_1 = "First Header Message Example";
 private final static String MESSAGE_2 = "Second Header Message Example";
 private final static String MESSAGE_3 = "Third Header Message Example";
  
 public void publish(){
   Map<String,Object> map = null;
   BasicProperties props = null; 
   try{
       Connection conn = RabbitMQConnection.getConnection();
       if(conn != null){
         Channel channel = conn.createChannel();
         
         //Notifications message
         sendMessageNotifications(channel, "{\"tittle\": \"Se ha recibido un pago\", \"message\": \"Usted ha recibido un pago de {} en su cuenta OnePay.\", \"params\":[\"20€\"]}");
         
         // First message 
//         props = new BasicProperties();
//         map = new HashMap<String,Object>(); 
//         map.put("First","A");
//         map.put("Fourth","D");
//         props = props.builder().headers(map).build();
//         channel.basicPublish(HeaderExchange.EXCHANGE_NAME, HeaderExchange.ROUTING_KEY, props, MESSAGE_1.getBytes());
//         System.out.println(" Message Sent '" + MESSAGE_1 + "'");
  
         // Second message 
//         props = new BasicProperties();
//         map = new HashMap<String,Object>(); 
//         map.put("Third","C");
//         props = props.builder().headers(map).build();
//         channel.basicPublish(HeaderExchange.EXCHANGE_NAME, HeaderExchange.ROUTING_KEY, props, MESSAGE_2.getBytes());
//         System.out.println(" Message Sent '" + MESSAGE_2 + "'");
  
         // Third message 
//         map = new HashMap<String,Object>();
//         props = new BasicProperties();
//         map.put("First","A");
//         map.put("Third","C");
//         props = props.builder().headers(map).build();
//         channel.basicPublish(HeaderExchange.EXCHANGE_NAME, HeaderExchange.ROUTING_KEY, props, MESSAGE_3.getBytes());
//         System.out.println(" Message Sent '" + MESSAGE_3 + "'");
  
         channel.close();
         conn.close();
       }
   }catch(Exception e){
       e.printStackTrace();
   }
 }

 private void sendMessageNotifications(Channel channel, String message) throws IOException{
	   Map<String,Object> map = new HashMap<String,Object>(); 
	   BasicProperties properties = new BasicProperties(); 
	   map.put("x-match", "any"); //any or all
	   map.put("content-type", MediaType.APPLICATION_JSON_VALUE);
	   map.put("userId", UUID.randomUUID().toString());
//	   map.put("paymentId", java.util.UUID.randomUUID());
       properties = properties.builder().headers(map).build();
       channel.basicPublish(HeaderExchange.EXCHANGE_NAME, HeaderExchange.ROUTING_KEY, properties, message.getBytes("UTF-8"));
       System.out.println(" Message Sent '" + message + "'");
 }

}
