package com.santander.integration.jms;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

import com.santander.integration.domain.Customer;

public class CustomerMessageConverter implements MessageConverter{

	public Message toMessage(Object object, Session session) throws JMSException, MessageConversionException {		
		Customer customer = (Customer) object;
		MapMessage message = session.createMapMessage();
		message.setInt("id", customer.getId());
		message.setString("name", customer.getName());
		message.setInt("age", customer.getAge());
		return message;
	}

	public Object fromMessage(Message message) throws JMSException,
			MessageConversionException {
		MapMessage mapMessage = (MapMessage) message;
		Customer customer = new Customer(mapMessage.getInt("id"), mapMessage.getString("name"), mapMessage.getInt("age"));
		return customer;
	}

}