package com.santander.integration.jms;

import org.springframework.messaging.Message;

public class ReadJMSQueue {
	
	public void readJMS(Message<?> msg) {
		
		for (int i = 0; i<= 100000000; i++){
			if ((i % 10000000) == 0){

				System.out.println("Log JMS Content timer: "+ i);
			}
		}
		
		System.out.println("Log JMS Content: "+ msg.getPayload());
	}
	
}
