package com.santander.integration.jms;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.santander.integration.BsnIntegrationApplication;
import com.santander.integration.domain.Customer;

public class SpringJmsProducer {
	
	private static Log log = LogFactory.getLog(SpringJmsProducer.class);
	
	private JmsTemplate jmsTemplate;
	private Destination destination;
	
	public JmsTemplate getJmsTemplate() {
		return jmsTemplate;
	}

	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}
	
	public Destination getDestination() {
		return destination;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	public void sendMessage(final String msg) {
		System.out.println("Producer sends " + msg);
		jmsTemplate.send(destination, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				return session.createTextMessage(msg);
			}});
	}
	
	public void sendCustomer(Customer customer) {
//		System.out.println("Producer sends " + customer);
		jmsTemplate.convertAndSend(destination, customer);
	}
	
	public void printCustomerToLog(Customer customer) {
        log.info("Customer: " + customer + " procesado.");
    }
}
