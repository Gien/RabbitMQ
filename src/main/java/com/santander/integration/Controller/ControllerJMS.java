package com.santander.integration.Controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.santander.integration.domain.Customer;
import com.santander.integration.jms.SpringJmsProducer;
import com.santander.integration.rabitmq.HeaderExchange;
import com.santander.integration.rabitmq.Producer;
import com.santander.integration.rabitmq.Receiver;

@RestController
@ImportResource({"classpath*:jms-integration.xml"})
public class ControllerJMS {
	
	@Autowired
	private SpringJmsProducer springJmsProducer;
	
	@PostConstruct
	public void init() throws Exception{
//		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("jms-integration.xml");
//		springJmsProducer = (SpringJmsProducer) applicationContext.getBean("springJmsProducer");
//		Customer customer = new Customer(1, "Juan", 24);
//		this.send(customer);
	}
	
	@RequestMapping(value="/addJMS", 
			consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE, 
			method=RequestMethod.POST)
	public void addDataJMS(@RequestBody Customer customer) throws Exception{
		this.send(customer);
	}
	
	@RequestMapping(value = "/getDate", method = RequestMethod.GET, produces = "application/json")
	public String getDate(){
		return "{ \"result\": \"hola! son " + new Date().toString()+"\"}";
	}

	private final static String QUEUE_NAME = "NOTIFICATIONS";
	private final static String ROUTING_KEY = "";
	  
	private void send(Customer customer) throws Exception{
//		springJmsProducer.sendCustomer(customer);
		
//	    ConnectionFactory factory = new ConnectionFactory();
//	    factory.setHost("localhost");
//	    Connection connection = factory.newConnection();
//	    Channel channel = connection.createChannel();
//	    
//	    Map<String, Object> bindingArgs = new HashMap<String, Object>();
//	    bindingArgs.put("x-match", "any"); //any or all
//	    bindingArgs.put("content-type", MediaType.APPLICATION_JSON_VALUE);
//	    bindingArgs.put("paymentId", java.util.UUID.randomUUID());
//
//	    BasicProperties properties = new BasicProperties.Builder().headers(bindingArgs).build();
////				.correlationId(java.util.UUID.randomUUID().toString()).build(); 
////	    channel.exchangeDeclare("Exchange_Header", "headers", true);
//	    //channel.queueBind(QUEUE_NAME, "Exchange_Header", ROUTING_KEY, bindingArgs);
//	    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
//	    String message = "{\"message\": \"Se ha recibido un pago\"}";
//	    channel.basicPublish("", QUEUE_NAME, properties, message.getBytes("UTF-8"));
//	    System.out.println(" [x] Sent '" + message + "'");
//
//	    channel.close();
//	    connection.close();
//	    
	    try{
	        HeaderExchange ex = new HeaderExchange();
	        ex.createExchangeAndQueue();
	    
	        // Publish
	        Producer produce = new Producer();
	        produce.publish();
	    
	        // Consume
	        Receiver receive = new Receiver();
	        receive.receive();
	     }catch(Exception e){
	       e.printStackTrace();
	     }
	}
}
