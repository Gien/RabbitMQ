package com.santander.integration;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

@SpringBootApplication
@EnableAutoConfiguration
public class BsnIntegrationApplication {
	
	private static Logger LOGGER = LoggerFactory.getLogger(BsnIntegrationApplication.class);

	public static void main(String[] args) throws UnknownHostException {
        final SpringApplication app = new SpringApplication(BsnIntegrationApplication.class);

        final Environment environment = app.run(args)
                .getEnvironment();
        String protocol = "http";
        if (environment.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        LOGGER.info(
                "\n----------------------------------------------------------\n\t"
                        + "Application '{}' is running! Access URLs:\n\t" + "Local: \t\t{}://localhost:{}\n\t"
                        + "External: \t{}://{}:{}\n\t"
                        + "Profile(s): \t{}\n----------------------------------------------------------",
                environment.getProperty("spring.application.name"), protocol, environment.getProperty("server.port"),
                protocol, InetAddress.getLocalHost()
                        .getHostAddress(),
                environment.getProperty("server.port"), environment.getActiveProfiles());
		
	}
}